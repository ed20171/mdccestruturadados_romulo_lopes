import scala.collection.mutable.ListBuffer

class HashTable(len: Int) {
  val size = len / 2;
  var elements: Array[ListBuffer[Key]] = null

  /**
   * Calcula a "Função" Hash para a exercício.
   * @param key chave a ser calculada
   * @return resultado da função hash
   */
  def hashFunction(key: Int): Int = {
    (key % size)
  }

  /**
   * Cria uma Tabela hash vazia, para cada elemento da Hash é criada uma ListBuffer
   */
  def create() {
    elements = Array.fill(size) { new ListBuffer[Key] }
  }

  /**
   * Insere un novo elemento na tabela hash
   * @param key Nova chave
   * @param value Objeto que será armazenado
   */
  def insert(key: Int, value: Object) {
    var item = new Key(key, value);
    val id = hashFunction(key)
    elements(id) += item;
  }

  /**
   * Procura um elemento na tabela hash.
   * @param key chave a ser procurada
   * @return Um Option com um Objeto Key ou None
   */
  def find(key: Int): Option[Key] = {
    val id = hashFunction(key)
    elements(id).find(x => x.key == key)
  }

  /**
   * Remove um deteminado elemento da tabela Hash.
   * @param key chave do elemento que será removido
   */
  def remove(key: Int) {
    val item = find(key)

    if (item != None) {
      val id = hashFunction(key)
      elements(id) -= item.get;
    }
  }

  override def toString(): String = {
    var str = ""
    elements.foreach(l => {
      str += "["
      l.foreach(i => {
        str += "(" + i.key + "|" + i.value + ")"
      })
      str += "]\n"
    })
    str
  }

  /**
   * Libera a lista.
   */
  def free() {
    elements.foreach { x => null }
    elements = null
  }
}

object MainHashTable extends App {
  println("Lista 03")
  println("----------")

  var h = new HashTable(10);

  println("1- Criar uma HashTable vazia")
  h.create()
  println("----------")

  println("2- Inserindo Elementos: 0 até 9")
  0 to 9 foreach{i => h.insert(i, i.toString())}
  println("----------")

  println("3- Imprimindo valores")
  println(h)
  println("----------")

  println("5- Procurando chave 2")
  var key = h.find(2)
  println(key)
  println("----------")

  println("6- Removendo chave 3")
  h.remove(3)
  println(h)
  println("----------")

  println("7- Liberando tabela hash")
  h.free()
  println("----------")

}