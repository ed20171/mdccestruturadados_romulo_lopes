

class Key(k: Int, v: Object) {
  var key = k;
  var value = v;

  override def toString(): String = {
    "[" + this.key + " | " + this.value.toString() + "]";
  }
}
