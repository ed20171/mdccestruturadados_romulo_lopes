import scala.collection.mutable.ListBuffer

class Heap(max: Int) {
  final val INVALID: Int = (-1)
  var elements: Array[Int] = null
  var last: Int = 0;

  /**
   * Verifica se um elemento é válido (>=0)
   * @param index indice do elemento
   * @return verdadeiro se é valido
   */
  def isValid(index: Int): Boolean = {
    index >= 0
  }

  /**
   * Retorna o indice do pai de um determinado elemento
   * @param index indice do elemento
   * @return indice do pai
   */
  def parent(index: Int): Int = {
    (index - 1) / 2
  }

  /**
   * Retorna o indice do filho esquerdo de um determinado elemento
   * @param index indice do elemento
   * @return indice do filho esquerdo
   */
  def left(index: Int): Int = {
    (index * 2) + 1
  }

  /**
   * Retorna o indice do filho direito de um determinado elemento
   * @param index indice do elemento
   * @return indice do filho direito
   */
  def right(index: Int): Int = {
    (index * 2) + 2
  }

  /**
   * Cria uma Heap vazia de tamanho max e todos com valor INVALID
   */
  def create() {
    elements = Array.fill(max) { INVALID }
  }

  /**
   * Cria uma Heap vazia de tamanho max e todos com valor INVALID
   */
  def swap(a: Int, b: Int) {
    var elem = elements(a)
    elements(a) = elements(b)
    elements(b) = elem
  }

  /**
   * Corrige os elementos acima
   */
  def fixUp(index: Int = last, upLast: Boolean = true) {
    var flag = true
    var pos: Int = index

    while (pos > 0 && flag) {
      var par: Int = 0
      if (upLast) {
        par = parent(last)
      } else {
        par = parent(pos)
      }

      if (elements(par) > elements(pos)) {
        swap(par, pos)
      } else {
        flag = false
      }

      pos = par;
    }
  }

  /**
   * Insere un novo elemento
   * @param value novo valor
   */
  def insert(value: Int) {
    if (last < max - 1) {
      elements(last) = value
      fixUp()
      last += 1
    } 
  }

  /**
   * Procura um elemento.
   * @param elem valor a ser procurado
   * @return Um Option com um Int ou None
   */
  def find(elem: Int): Option[Int] = {
    elements.find(_ == elem)
  }

  /**
   * Corrige elementos abaixo
   */
  def fixDown() {
    var parent: Int = 0
    var flag: Boolean = true

    while (2 * parent + 1 < last && flag) {
      var leftS: Int = left(parent)
      var rightS: Int = right(parent)
      var son: Int = 0

      if (rightS >= last) {
        rightS = leftS;
      }

      if (elements(leftS) < elements(rightS)) {
        son = leftS;
      } else {
        son = rightS;
      }

      if (elements(parent) > elements(son)) {
        swap(parent, son);
      } else {
        flag = false
      }
      parent = son;
    }
  }

  /**
   * Remove um elemento
   * @return chave removida
   */
  def remove(): Int = {
    if (last > 0) {
      var topo: Int = elements(0);
      elements(0) = elements(last - 1)
      elements(last - 1) = INVALID
      last -= 1
      fixDown();
      return topo;
    } else {
      INVALID;
    }
  }

  /**
   * Altera um determinado elemento
   * @param old valor antigo do elemento
   * @param new valor novo do elemento
   */
  def change(oldE: Int, newE: Int) {
    for (i <- 0 until last) {
      if (elements(i) == oldE) {
        elements(i) = newE
        //down
        if ((isValid(left(i)) || isValid(right(i))) &&
          ((newE > elements(left(i)) && isValid(elements(left(i)))) ||
            (newE > elements(right(i)) && isValid(elements(right(i)))))) {
          fixDown()
          //up
        } else if (isValid(parent(i)) && newE < elements(parent(i))) {
          fixUp(i, false)
        }
      }
    }
  }

  override def toString(): String = {
    var ret = ""
    var lineWidth = 100
    var out: Array[Int] = new Array(last)
    var j = 1
    var k = 0
    var pos = 0
    var x = 1
    var level = 0

    out(0) = 0;
    for (i <- 0 until last) {
      pos = out(parent(i))
      if (i % 2 == 0) {
        pos += (lineWidth / (scala.math.pow(2, level + 1) + 1)).toInt
      } else {
        pos -= (lineWidth / (scala.math.pow(2, level + 1) + 1)).toInt
      }

      for (k <- 0 until pos - x) {
        if (i == 0 || i % 2 != 0)
          ret += ' '
        else
          ret += '-'
      }

      ret += elements(i)

      out(i) = pos + 1
      x = pos + 1
      if (j == scala.math.pow(2, level)) {
        ret += ("\n")
        level += 1;
        x = 1
        j = 0
      }

      j += 1
    }

    ret
  }

  /**
   * Libera a lista.
   */
  def free() {
    elements = null
  }
}

object MainHeap extends App {
  println("Lista 05")
  println("============================================\n")

  var h = new Heap(20)

  println("1- Criar uma Heap vazia")
  h.create()
  println("============================================\n")

  println("2- Inserindo Elementos: 0 até 9")
  0 to 9 foreach { i => h.insert(i) }
  println("============================================\n")

  println("3- Imprimindo valores")
  println(h)
  println("============================================\n")

  println("4- Removendo elementos ")
  println("Removeu: " + h.remove())
  println("Removeu: " + h.remove())
  println("Removeu: " + h.remove())
  println(h)
  println("============================================\n")

  println("5- Mudando 3 para 11")
  println(h)
  h.change(3, 11)
  println(h)
  println("============================================\n")

  println("6- Mudando 9 para 0")
  println(h)
  h.change(9, 0)
  println(h)
  println("============================================\n")

  println("7- Liberando a Heap")
  h.free()
  println("============================================\n")
}