import scala.collection.mutable.BitSet
import scala.collection.mutable.ListBuffer

class Partition(size: Int) {
  val INVALID: Int = -1
  var elements: Array[Int] = null

  /**
   * Cria uma um conjunto vazio
   */
  def create() {
    elements = new Array[Int](size)

    for (i <- 0 until size) {
      makeset(i)
    }
  }

  def makeset(x: Int) {
    if (x < size) {
      elements(x) = -1
    }
  }

  /**
   * Faz a busca do representante de um elemento
   * @param x elemento a para ser buscado
   * @return representante do conjunto
   */
  def find(x: Int): Int = {
    if (x < size) {
      if (elements(x) == INVALID) {
        x
      } else {
        find(elements(x))
      }
    } else {
      INVALID
    }
  }

  /**
   * Realiza a união entre dois conjuntos
   * @param x primeiro elemento
   * @param y segundo elemento
   */
  def union(x: Int, y: Int) {
    if (x < size && y < size) {
      val fx = find(x)
      val fy = find(y)

      elements(fx) = fy
    }
  }

  /**
   * Verifica se dois elementos pertencem ao mesmo conjunto.
   * @param x primeiro elemento
   * @param y segundo elemento
   * @return verdadeiro caso os elementos @param x e @param y percentem ao mesmo conjunto
   */
  def isSameSet(x: Int, y: Int): Boolean = {
    val fx = find(x)
    val fy = find(y)

    (fx == fy) && (fx != INVALID) && (fy != INVALID)
  }

  override def toString(): String = {
    var list: Array[ListBuffer[Int]] = Array.fill(size) { new ListBuffer[Int] }
    var ret = ""

    for (i <- 0 until size) {
      list(find(i)) += i
    }

    for (i <- 0 until size) {

      if (list(i).size > 0) {
        ret += "["
      }

      for (j <- 0 until list(i).size) {
        if (j == list(i).size - 1) {
          ret += "'" + list(i)(j) + "'"
        } else {
          ret += list(i)(j)
        }

        if (j < list(i).size - 1) {
          ret += " , "
        }
      }
      if (list(i).size > 0) {
        ret += "]  "
      }
    }

    ret
  }

  /**
   * Libera a lista.
   */
  def free() {
    elements = null
  }
}

object MainPartition extends App {
  println("Lista 07")
  println("============================================\n")

  println("============================================\n")
  println("Criando uma partição com 10 elementos (0-9)")
  var partition = new Partition(10)
  partition.create()
  println("Conjuntos:" + partition)

  println("============================================\n")
  println("União entre [0 , 1]  e [4, 5, 6]")
  partition.union(0, 1)

  partition.union(4, 5)
  partition.union(4, 6)

  println("Conjuntos:" + partition)

  println("============================================\n")
  println("Representante de 4 :" + partition.find(4))
  println("Representante de 0 :" + partition.find(0))
  println("Representante de 6 :" + partition.find(6))
  println("Representante de 8 :" + partition.find(8))

  println("============================================\n")
  println("Elementos 4 e 5 estão no mesmo conjunto: " + partition.isSameSet(4, 5))
  println("Elementos 0 e 1 estão no mesmo conjunto: " + partition.isSameSet(0, 1))
  println("Elementos 1 e 6 estão no mesmo conjunto: " + partition.isSameSet(1, 6))

  println("============================================\n")

  println("Liberando estrutura")
  partition.free()

}