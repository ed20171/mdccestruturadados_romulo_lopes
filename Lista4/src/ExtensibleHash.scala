import scala.collection.mutable.ListBuffer

class ExtensibleHash(len: Int, bucketLen: Int) {
  var numBits: Int = 0
  var buckets: ListBuffer[ListBuffer[Key]] = null
  var elements: ListBuffer[(Int, ListBuffer[Key])] = null

  /**
   * Cria uma Tabela hash vazia, para cada elemento da Hash é criada uma ListBuffer
   */
  def create() {
    elements = new ListBuffer[(Int, ListBuffer[Key])]
    buckets = new ListBuffer[ListBuffer[Key]]

    numBits = 1

    var bucket0 = new ListBuffer[Key];
    var bucket1 = new ListBuffer[Key];

    buckets += bucket0
    buckets += bucket1

    var i = (0, bucket0);
    elements += i
    var j = (1, bucket1);
    elements += j
  }

  /**
   * Calcula a "Função" Hash para a exercício.
   * @param key chave a ser calculada
   * @return resultado da função hash
   */
  def hashFunction(key: Int): Int = {
    (key % len)
  }

  /**
   * Baseado na chave, retorna o bucket correspondente da chave;
   * @param key chave para encontrar o bucket
   * @return Bucket ao qual a chave pertence
   */
  def getBucket(key: Int): ListBuffer[Key] = {
    val hKey = hashFunction(key)
    var elem = elements.find({ x => x._1 == mask(hKey) }).get
    elem._2
  }

  /**
   * Calcula a mascara para um determinado numero
   * @param value valor pra ser calculado.
   * @return mascara calculada
   */
  def mask(value: Int): Int = {
    value & (1 << (numBits)) - 1
  }

  /**
   * Insere un novo elemento na tabela hash
   * @param key Nova chave
   * @param value Objeto que será armazenado
   */
  def insert(key: Int, value: Object) {
    var item = new Key(key, value);

    var bucket = getBucket(key)
    bucket += item
    if (bucket.length > bucketLen) {

      if (buckets.length >= (1 << numBits)) {
        numBits += 1

        //copia primeira metade da lista, para a segunda, 
        //mudando os indices
        var tempElem = elements.clone()
        tempElem.foreach(x => tempElem(x._1) = (x._1 + elements.size, x._2))
        elements ++= tempElem
      }

      //remove o bucket cheio
      buckets -= bucket

      //divide em duas partições, baseado na mascara
      var parts = bucket.partition(x => mask(x.key) == mask(key))

      //adiciona os novos buckets
      buckets += parts._1
      buckets += parts._2

      val oldMask = (1 << (numBits - 1)) - 1

      val idx1 = key & oldMask
      val idx2 = idx1 + (1 << numBits - 1)

      //atualiza os indices para os novos buckets
      elements(idx1) = (idx1, parts._1)
      elements(idx2) = (idx2, parts._2)
    }
  }

  /**
   * Procura um elemento na tabela hash.
   * @param key chave a ser procurada
   * @return Um Option com um Objeto Key ou None
   */
  def find(key: Int): Option[Key] = {
    var bucket = getBucket(key)
    bucket.find(x => x.key == key)
  }

  /**
   * Remove um deteminado elemento da tabela Hash.
   * @param key chave do elemento que será removido
   */
  def remove(key: Int) {
    val item = find(key)

    if (item != None) {
      var bucket = getBucket(key)
      bucket -= item.get;
    }
  }

  override def toString(): String = {
    var str = ""
    var i: Int = 0

    buckets.foreach(l => {
      str += "[bucket " + i + ": "
      l.foreach(i => {
        str += "(" + i.key + "|" + i.value + ")"
      })
      str += "]\n"
      i += 1
    })
    str
  }

  /**
   * Libera a lista.
   */
  def free() {
    elements.foreach { x => null }
    elements = null

    buckets.foreach { x => null }
    buckets = null
  }
}

object MainExtensibleHash extends App {
  println("Lista 04")
  println("----------")

  var h = new ExtensibleHash(10, 4);

  println("1- Criar uma Hash extensiva vazia")
  h.create()
  println("----------")

  println("2- Inserindo Elementos: 0 até 20")
  0 to 9 foreach { i => h.insert(i, i.toString()) }
  println("----------")

  println("3- Imprimindo valores")
  println(h)
  println("----------")

  println("5- Procurando chave 2")
  var key = h.find(2)
  println(key)
  println("----------")

  println("6- Removendo chave 3")
  h.remove(3)
  println(h)
  println("----------")

  println("7- Liberando tabela hash")
  h.free()
  println("----------")

}