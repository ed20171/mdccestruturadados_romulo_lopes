import scala.collection.mutable.Queue
import scala.collection.mutable.Stack

class BinaryTree(v:Int,l:BinaryTree= null,r:BinaryTree=null) extends Node[BinaryTree](v,l,r){
  
  /**
   * Print nodes.
   */
  def print(){
    val queue = new Queue[BinaryTree]()
    queue.enqueue(this)
    
    while (!queue.isEmpty){
      val node = queue.dequeue()
      println(node.value)

      if (node.left != null){
        queue.enqueue(node.left)
      }
      if (node.right != null){
        queue.enqueue(node.right)
      }
    }
  }

  /**
   * Questão 09
   * Print nodes, leaf to Root 
   */
  def printLeafToRoot(){
    var stack = new Stack[BinaryTree]()
    val queue = new Queue[BinaryTree]()
    queue.enqueue(this)
    
    while (!queue.isEmpty){
      val node = queue.dequeue()
      stack.push(node)

      if (node.left != null){
        queue.enqueue(node.left)
      }
      if (node.right != null){
        queue.enqueue(node.right)
      }
    }

    while (!stack.isEmpty){
      println(stack.pop().value)
    }
  }

  /**
   * Questão 12
   * Premute left to Right
   */
  def permuteLeftRight(){
    val rightNode = right
    right = left
    left = rightNode 
    if (left != null){
      left.permuteLeftRight()
    }
    if (right != null){
      right.permuteLeftRight()
    }
  }
  
}
