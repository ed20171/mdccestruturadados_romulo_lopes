
class Node[T](v:Int,l:T= null,r:T=null){
  var left:T = l;
  var right:T = r;
  var value:Int = v;
}