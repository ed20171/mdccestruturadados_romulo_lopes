

object main extends App {

  println("============================================\n")
  println("Lista 09- BinaryTree")
  println("============================================\n")

  println("============================================\n")
  println("Criando arvore")
  var tree = new BinaryTree(1)
  tree.left = new BinaryTree(2)
  tree.right = new BinaryTree(3)
  tree.left.left = new BinaryTree(4)
  tree.left.right = new BinaryTree(5)
  tree.right.left = new BinaryTree(6)
  tree.right.right = new BinaryTree(7)
  
  println("============================================\n")  
  println("Questão 09")
  tree.printLeafToRoot()
  
  
  println("============================================\n")  
  println("Questão 12")
  println("Antes")
  tree.print()
  tree.permuteLeftRight()
  println("Depois")
  tree.print()
  
  
  
  println("============================================\n")
  println("Lista 09- BinarySearchTree")
  println("============================================\n")
  
  
  println("============================================\n")
  println("Criando arvore")
  var serchTree = new BinarySearchTree(8)
  serchTree.insert(2)
  serchTree.insert(3)
  serchTree.insert(4)
  serchTree.insert(9)
  serchTree.insert(10)
  serchTree.insert(11)
  
  println("============================================\n")
  println("Questão 10")
  println("Procurando (2): " + serchTree.find(2))
  
  
  println("============================================\n")
  println("Questão 11")
  println("max: " + serchTree.max)
  
  
  println("============================================\n")
  println("Questão 12")
  println("Antecessor de (4) :" + serchTree.predecessor(4))
  
  
  
}