import scala.collection.mutable.ListBuffer
import scala.collection.mutable.Queue

class BinarySearchTree(v: Int, l: BinarySearchTree = null, r: BinarySearchTree = null) extends Node[BinarySearchTree](v, l, r) {

  /**
   * Insert new element.
   */
  def insert(value: Int): Unit = {
    if (this.value > value) {
      if (left != null) {
        left.insert(value)
      } else {
        left = new BinarySearchTree(value)
      }
    } else {
      if (right != null) {
        right.insert(value)
      } else {
        right = new BinarySearchTree(value)
      }
    }
  }

  /**
   * Questão 10.
   * Find a value, if it exists.
   *
   */
  def find(elem: Int): Int = {
    if (this.value == elem) {
      return this.value
    }
    if (this.value > elem) {
      if (left != null) left.find(elem) else -1
    }else{
      if (right != null) right.find(elem) else -2
    }
  }

  /**
   * Questão 11.
   * Get max value
   */
  def max(): Int = {
    if (right == null)
      this.value
    else
      right.max()
  }
  
  /*
   * Predecessor of a element value
   */
  def predecessor(elem:Int): Int={
    val queue = new Queue[BinarySearchTree]()
    val preds = new ListBuffer[Int]()
    queue.enqueue(this)
    
    while (!queue.isEmpty){
      val node = queue.dequeue()
      if(node.value < elem) preds += node.value

      if (node.left != null){
        queue.enqueue(node.left)
      }
      if (node.right != null){
        queue.enqueue(node.right)
      }
    }
    
    if( preds.size >0){
      preds.max
    }else{
      -1
    }
    
  }
}

