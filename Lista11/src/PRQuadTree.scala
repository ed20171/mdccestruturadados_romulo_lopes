
class Node(xv: Int, yv: Int, v: Int) {
  var x: Int = xv
  var y: Int = yv
  var NW: Node = null
  var NE: Node = null
  var SE: Node = null
  var SW: Node = null
  var value: Int = v

  def eq(n: Node): Boolean = {
    x == n.x && y == n.y && value == n.value
  }

  override def toString(): String = {
    "(" + x + "," + y + ")=" + value
  }

}

class PRQuadTree(width: Int, height: Int) {
  var root: Node = null

  def create() {
    root = null
  }

  def insert(x: Int, y: Int, v: Int) {
    if (root == null) {
      root = new Node(x, y, v)
    } else {
      insert(new Node(x, y, v), root, 1)
    }
  }

  private def menorWidth(x: Int, sub: Int): Boolean = {
    x < (width / 2 << sub)
  }

  private def menorHeight(y: Int, sub: Int): Boolean = {
    y < (height / 2 << sub)
  }

  private def insert(n: Node, r: Node, sub: Int) {
    if (menorWidth(n.x, sub) && menorHeight(n.y, sub)) { //sw
      if (r.SW == null) {
        r.SW = n
      } else {
        insert(n, r.SW, sub + 1)
      }
    } else if (menorWidth(n.x, sub) && !menorHeight(n.y, sub)) { //nw
      if (r.NW == null) {
        r.NW = n
      } else {
        insert(n, r.NW, sub + 1)
      }
    } else if (!menorWidth(n.x, sub) && menorHeight(n.y, sub)) { //se
      if (r.SE == null) {
        r.SE = n
      } else {
        insert(n, r.SE, sub + 1)
      }
    } else { //ne
      if (r.NE == null) {
        r.NE = n
      } else {
        insert(n, r.NE, sub + 1)
      }
    }
  }

  def find(n: Node, r: Node = root , sub: Int = 1): Node = {
    if (r == null || r.eq(n)) {
      return r
    } else {
        if (menorWidth(n.x, sub) && menorHeight(n.y, sub)) { //sw
          if (r.SW != null) {
            if (n.eq(r.SW)) {
              return r.SW 
            }else{
              find(n, r.SW, sub + 1)
            }
          }
        } else if (menorWidth(n.x, sub) && !menorHeight(n.y, sub)) { //nw
          if (r.NW != null) {
           if (n.eq(r.NW)) {
              return r.NW 
            }else{
              find(n, r.NW, sub + 1)
            }
            
          } 
        } else if (!menorWidth(n.x, sub) && menorHeight(n.y, sub)) { //se
          if (r.SE != null) {
            if (n.eq(r.SE)) {
              return r.SE 
            }else{
              find(n, r.SE, sub + 1)
            }
          }
        } else { //ne
          if (r.NE != null) {
            if (n.eq(r.NE)) {
              return r.NE 
            }else{
              find(n, r.NE, sub + 1)
            }
          }
        }
      }
      null
  }
  

  def remove(n: Node, r: Node = root , sub: Int = 1) {
    if (menorWidth(n.x, sub) && menorHeight(n.y, sub)) { //sw
      if (r.SW != null) {
        if (n.eq(r.SW)) {
          r.SW = null
        }else{
          remove(n, r.SW, sub + 1)
        }
      }
    } else if (menorWidth(n.x, sub) && !menorHeight(n.y, sub)) { //nw
      if (r.NW != null) {
       if (n.eq(r.NW)) {
          r.NW = null
        }else{
          remove(n, r.NW, sub + 1)
        }
        
      } 
    } else if (!menorWidth(n.x, sub) && menorHeight(n.y, sub)) { //se
      if (r.SE != null) {
        if (n.eq(r.SE)) {
          r.SE = null
        }else{
          remove(n, r.SE, sub + 1)
        }
      }
    } else { //ne
      if (r.NE != null) {
        if (n.eq(r.NE)) {
          r.NE = null
        }else{
          remove(n, r.NE, sub + 1)
        }
      }
    }
  }
    

}

object mainPrQuadTree extends App {

  println("============================================\n")
  println("1. Criar uma PR-Quadtree vazia")
  var quad = new PRQuadTree(10, 10)
  quad.create()

  println("============================================\n")
  println("2. Inserir (1,1)=3, (1,2)=2, (2,1)=1")
  quad.insert(1, 1, 3)
  quad.insert(1, 2, 2)
  quad.insert(2, 1, 1)

  println("============================================\n")
  println("3. Remover ")
  quad.remove(new Node(2, 1, 1))

  println("============================================\n")
  println("4. Pesquisar (1,2)=2")
  println(quad.find(new Node(1, 2, 2)))

  println("End")

}
