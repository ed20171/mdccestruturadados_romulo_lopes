
class Node_(xv: Int, yv: Int, v: Int) {
  var x: Int = xv
  var y: Int = yv
  var left: Node_ = null
  var right: Node_ = null
  var value: Int = v

  def eq(n: Node_): Boolean = {
    x == n.x && y == n.y && value == n.value
  }

  override def toString(): String = {
    "(" + x + "," + y + ")=" + value
  }

}

class PRKDTree(width: Int, height: Int) {
  var root: Node_ = null

  def create() {
    root = null
  }

  def insert(x: Int, y: Int, v: Int) {
    if (root == null) {
      root = new Node_(x, y, v)
    } else {
      insert(new Node_(x, y, v), root, 1)
    }
  }

  private def menor(n1: Node_, sub: Int): Boolean = {
    if (sub % 2 == 0) {
      n1.y < (height / 2 << sub)
    } else {
      n1.x < (width / 2 << sub)
    }
  }

  private def insert(n: Node_, r: Node_, sub: Int) {
    if (menor(n, sub)) { //sw
      if (r.left == null) {
        r.left = n
      } else {
        insert(n, r.left, sub + 1)
      }
    } else {
      if (r.right == null) {
        r.right = n
      } else {
        insert(n, r.right, sub + 1)
      }
    }
  }

  def find(n: Node_, r: Node_ = root, sub: Int = 1): Node_ = {
    if (r == null || r.eq(n)) {
      return r
    } else {
      if (menor(n, sub)) { //left
        if (r.left != null) {
          if (n.eq(r.left)) {
            return r.left
          } else {
            find(n, r.left, sub + 1)
          }
        }
      } else {
        if (r.right != null) {
          if (n.eq(r.right)) {
            return r.right
          } else {
            find(n, r.right, sub + 1)
          }
        }
      }
    }

    null
  }

  def remove(n: Node_, r: Node_ = root, sub: Int = 1) {
    if (menor(n, sub)) { //left
      if (r.left != null) {
        if (n.eq(r.left)) {
          r.left = null
        } else {
          remove(n, r.left, sub + 1)
        }
      }
    } else {
      
      if (r.right != null) {
        if (n.eq(r.right)) {
          r.right = null
        } else {
          remove(n, r.right, sub + 1)
        }
      }
      
    }
  }
}

object mainPrKdTree extends App {

  println("============================================\n")
  println("1. Criar uma PR-KDtree vazia")
  var pr = new PRKDTree(10, 10)
  pr.create()

  println("============================================\n")
  println("2. Inserir (1,1)=3, (1,2)=2, (2,1)=1")
  pr.insert(1, 1, 3)
  pr.insert(1, 2, 2)
  pr.insert(2, 1, 1)

  println("============================================\n")
  println("3. Remover ")
  pr.remove(new Node_(2, 1, 1))

  println("============================================\n")
  println("4. Pesquisar (1,2)=2")
  println(pr.find(new Node_(1, 2, 2)))

  println("End")

}
