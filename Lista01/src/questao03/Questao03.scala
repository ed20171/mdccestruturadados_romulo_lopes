package questao03

class Node(n: Node, p: Node, v: Int) {
  var value = v
  var next = n
  var prev = p
}

trait AbstractDoublyOrderLinkedList {
  def create()
  def insert(value: Int)
  def print()
  def printRecursion()
  def printReverse()
  def isEmpty(): Int
  def find(value: Int): Int
  def remove(value: Int)
  def removeRecursion(value: Int)
  def free()
  def equals(list: DoublyOrderLinkedList): Boolean
}

class DoublyOrderLinkedList extends AbstractDoublyOrderLinkedList {
  var head: Node = null
  var tail: Node = null

  override def create() {
    head = null
    tail = null
  }

  override def insert(value: Int) = {
    var current: Node = head;
    var prev: Node = null;

    while (current != null && current.value < value) {
      prev = current;
      current = current.next;
    }

    if (prev == null) {
      var node = new Node(null, null, value);
      head = node;

      if (tail == null) {
        tail = head;
      }

    } else {
      var node = new Node(prev.next, prev, value);
      if (prev.next != null) {
        prev.next.prev = node;
      }

      prev.next = node;

      if (tail.value < value) {
        tail = node;
      }
    }
  }

  override def print() = {
    var temp: Node = head

    Console.printf("(")
    while (temp != null) {
      Console.printf(temp.value + ", ")
      temp = temp.next

    }
    Console.printf(")\n")
  }

  private def printRec(node: Node) {
    if (node != null) {
      Console.printf(node.value + ", ")
      printRec(node.next)
    }
  }

  override def printRecursion() {
    Console.printf("(")
    printRec(head)
    Console.printf(")\n")
  }

  private def printRev(node: Node) {
    if (node != null) {
      printRev(node.next)
      Console.printf(node.value + ", ")
    }
  }

  override def printReverse() {
    Console.printf("(")
    printRev(head)
    Console.printf(")\n")
  }

  override def isEmpty(): Int = {
    if (head == tail) {
      1
    }
    0
  }

  override def find(value: Int): Int = {
    var temp: Node = head
    var res: Int = 0
    while (temp != null) {
      if (temp.value == value) {
        res = value;
      }
      temp = temp.next;
    }
    res
  }

  override def remove(value: Int) = {
    var temp: Node = head
    var old: Node = null;

    while (temp != null) {
      if (temp.value == value) {

        if (old == null) {
          head = head.next;
        } else {
          old.next = temp.next;
          if (temp.next != null) {
            temp.next.prev = old;
          }
        }
      }

      old = temp;
      temp = temp.next;
    }
  }

  private def removeRec(old: Node, node: Node, value: Int) {
    if (node.value == value) {
      if (old == null) {
        head = head.next;
      } else {
        old.next = node.next;
      }
    } else {
      removeRec(node, node.next, value)
    }
  }

  override def removeRecursion(value: Int) = {
    var old: Node = null
    var node: Node = head

    removeRec(old, node, value);
  }

  override def free() = {
    var temp: Node = head
    var node: Node = head;
    while (temp != null) {
      temp = temp.next
      node.next = null;
      node.prev = null;

      node = temp
    }
  }

  def equals(list: DoublyOrderLinkedList): Boolean = {
    var headList1: Node = head
    var headList2: Node = list.head

    while (headList1 != null && headList2 != null) {
      if (headList1.value != headList2.value) {
        return false;
      }

      headList2 = headList2.next;
      headList1 = headList1.next;
    }

    if (headList1 == null && headList2 == null) {
      return true;
    } else {
      return false;
    }
  }
}

object Questao03 extends App {
  println("Questão 03")
  println("----------")

  var list: questao03.DoublyOrderLinkedList = new questao03.DoublyOrderLinkedList()
  println("1- Criar uma lista vazia")
  list.create()
  println("----------")

  println("2- Inserir elemento no início")
  println("Inserindo os elementos: (1,2,3)")
  list.insert(1)
  list.insert(3)
  list.insert(2)
  println("----------")

  println("3- Imprimir os valores armazenados na lista")
  list.print()
  println("----------")

  println("4- Imprimir os valores armazenados na lista usando recursão")
  list.printRecursion()
  println("----------")

  println("5- Imprimir os valores armazenados na lista em ordem reversa (da cauda para a cabeça da lista)")
  list.printReverse()
  println("----------")

  println("6- Verificar se a lista está vazia (retorna 1 se vazia ou 0 se não vazia)")
  println("isEmpty: (" + list.isEmpty() + ")")
  println("----------")

  println("7. Recuperar/Buscar um determinado elemento da lista")
  println("find 2: (" + list.find(2) + ")")
  println("----------")

  println("8. Remover um determinado elemento da lista")
  println("remove: 1")
  list.remove(1)
  list.print()
  println("----------")

  println("9. Remover um determinado elemento da lista usando recursão")
  println("remove: 2")
  list.removeRecursion(2)
  list.print()
  println("----------")

  println("10. Liberar a lista")
  list.free()
  println("----------")

  println("11. Verificar se duas listas são iguais")
  println("lista (3) é igual a lista(2,3)?")
  var list1: questao03.DoublyOrderLinkedList = new questao03.DoublyOrderLinkedList
  list1.create()
  list1.insert(3)

  var list2: questao03.DoublyOrderLinkedList = new questao03.DoublyOrderLinkedList
  list2.create()
  list2.insert(3)
  list2.insert(2)
  println(list1.equals(list2))

  println("----------")
}
