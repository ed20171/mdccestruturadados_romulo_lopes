package questao06

class ContaFidelidade(num: Double) extends Conta(num) {
  var bonus: Double = 0;
  def renderBonus() {
    saldo += bonus;
    bonus = 0;
  }

  override def creditar(valor: Double): Unit = {
    saldo += valor
    bonus += valor * 0.01;
  }

  override def toString(): String = {
    var res = ""
    res += "[Número: " + this.numero + " | ";
    res += "Saldo: " + this.saldo + " | ";
    res += "Bonus: " + this.bonus + "]";
    res
  }

}