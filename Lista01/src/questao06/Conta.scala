package questao06

class Conta(num: Double) {
  var numero: Double = num
  var saldo: Double = 0

  def creditar(valor: Double): Unit = {
    if (valor > 0) {
      saldo += valor
    }
  }

  def debitar(valor: Double): Boolean = {
    if(valor < 0 ){
        return false
    }
    
    if (saldo - valor >= 0) {
      saldo -= valor
      return true
    }
    return false
  }

  def transferir(destino: Conta, valor: Double) {
    if (this.debitar(valor)) {
      destino.creditar(valor)
    }
  }

  def <(value: Conta): Boolean = {
    this.numero < value.numero
  }
  def equals(value: Conta): Boolean = {
    (this.numero == value.numero)
  }

  def !=(value: Conta): Boolean = {
    (this.numero != value.numero)
  }

  override def toString(): String = {
    var res = ""
    res += "[Número: " + this.numero + " | ";
    res += "Saldo: " + this.saldo + "]";
    res
  }
}