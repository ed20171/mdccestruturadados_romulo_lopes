package questao06

object Questao06 extends App {
  def readDouble(msg: String): Option[Double] = {
    println(msg)

    var num: Double = 0
    try {
      num = scala.io.StdIn.readDouble()
      Some(num)
    } catch {
      case eofe: java.lang.NumberFormatException => {
        println("Valor inválido.")
        None
      }
    }
  }

  def getConta(msg: String = "Informe o número da Conta"): Conta = {
    var num = readDouble(msg)

    if (!num.isEmpty) {
      var conta = list.find(new Conta(num.get))
      if (!(conta == null)) {
        return conta
      } else {
        println("Conta não encontrada.")
      }
    }
    return null
  }

  def contaExiste(num: Double): Boolean = {
    var conta: Conta = list.find(new Conta(num))

    return !(conta == null)
  }

  var sel: Int = 0
  var list: questao06.OrderLinkedList = new questao06.OrderLinkedList()
  do {
    println("1. Inserir uma conta bancária")
    println("2. Inserir uma conta poupança;")
    println("3. Inserir uma conta fidelidade;")
    println("4. Realizar crédito em uma determinada conta;")
    println("5. Realizar débito em uma determinada conta;")
    println("6. Consultar o saldo de uma conta;")
    println("7. Consultar o bônus de uma conta fidelidade;")
    println("8. Realizar uma transferência entre duas contas;")
    println("9. Render juros de uma conta poupança;")
    println("10. Render bônus de uma conta fidelidade;")
    println("11. Remover uma conta;")
    println("12. Imprimir número e saldo de todas as contas cadastradas;")

    try {
      sel = scala.io.StdIn.readInt()
    } catch {
      case eofe: java.lang.NumberFormatException => { sel = -1 }
    }

    sel match {
      case 1 => {
        var num = readDouble("Informe o número da Conta.")

        if (!num.isEmpty) {
          if (!contaExiste(num.get)) {
            list.insert(new Conta(num.get))
          } else {
            println("Número da conta já existe.")
          }
        }
      }
      case 2 => {
        var num = readDouble("Informe o número da Conta.")

        if (!num.isEmpty) {
          if (!contaExiste(num.get)) {
            list.insert(new ContaPoupanca(num.get))
          } else {
            println("Número da conta já existe.")
          }
        }
      }
      case 3 => {
        var num = readDouble("Informe o número da Conta.")

        if (!num.isEmpty) {
          if (!contaExiste(num.get)) {
            list.insert(new ContaFidelidade(num.get))
          } else {
            println("Número da conta já existe.")
          }
        }
      }
      case 4 => {
        var conta = getConta()
        if (!(conta == null)) {
          var valor = readDouble("Informe o valor para creditar")
          if (!valor.isEmpty) {
            conta.creditar(valor.get);
          }
        }
      }

      case 5 => {
        var conta = getConta()
        if (!(conta == null)) {
          var valor = readDouble("Informe o valor para debitar")
          if (!valor.isEmpty) {
            conta.debitar(valor.get);
          }
        }
      }

      case 6 => {

        var conta = getConta()
        if (!(conta == null)) {
          println("(" + conta.toString() + ")")
        }
      }

      case 7 => {
        var conta = getConta()
        if (!(conta == null)) {
          if (conta.isInstanceOf[ContaFidelidade]) {
            var contaFidelidade = conta.asInstanceOf[ContaFidelidade];
            println("(" + contaFidelidade.toString() + ")")
          } else {
            println("Conta não é do tipo Fidelidade")
          }
        }
      }

      case 8 => {

        var conta1 = getConta("Informe o número da primeira conta (Debitar).")
        var conta2 = getConta("Informe o número da segunda conta (Creditar).")

        var valor = readDouble("Informe o valor da transferência.")

        if (!valor.isEmpty) {
          conta1.transferir(conta2, valor.get)
        }
      }
      case 9 => {
        var conta = getConta()
        if (!(conta == null)) {
          if (conta.isInstanceOf[ContaPoupanca]) {
            var contaPoupanca = conta.asInstanceOf[ContaPoupanca];
            var juros = readDouble("Informe o valor do juros.")

            if (!juros.isEmpty) {
              contaPoupanca.renderJuros(juros.get)
            }
          } else {
            println("Conta não é do tipo Poupanca")
          }
        }

      }
      case 10 => {
        var conta = getConta()
        if (!(conta == null)) {
          if (conta.isInstanceOf[ContaFidelidade]) {
            var contaFidelidade = conta.asInstanceOf[ContaFidelidade];
            contaFidelidade.renderBonus()
          } else {
            println("Conta não é do tipo Fidelidade")
          }
        }

      }

      case 11 => {
        var conta = getConta();
        if (!(conta == null)) {
          list.remove(conta)
        }
      }

      case 12 => {
        list.print()
      }

      case 0 => {}
      case _ => {
        println("Informe um valor válido!!!.")
      }
    }

  } while (sel != 0)

  list.free()

}