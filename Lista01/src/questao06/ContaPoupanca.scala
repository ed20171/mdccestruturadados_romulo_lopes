package questao06

class ContaPoupanca(num: Double) extends Conta(num) {
  def renderJuros(juros: Double) {
    if (juros > 0) {
      saldo += saldo * juros;
    }
  }
}