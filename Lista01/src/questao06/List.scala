package questao06

class Node(n: Node, v: Conta) {
  var value = v
  var next = n
}

trait AbstractOrderLinkedList {
  def create()
  def insert(value: Conta)
  def print()
  def printRecursion()
  def printReverse()
  def isEmpty(): Int
  def find(value: Conta): Conta
  def remove(value: Conta)
  def removeRecursion(value: Conta)
  def free()
  def equals(list: OrderLinkedList): Boolean
}

class OrderLinkedList extends AbstractOrderLinkedList {
  var head: Node = null
  var tail: Node = null

  override def create() {
    head = null
    tail = null
  }

  override def insert(value: Conta) = {
    var current: Node = head;
    var prev: Node = null;

    while (current != null && current.value < value) {
      prev = current;
      current = current.next;
    }

    if (prev == null) {

      var node = new Node(head, value);
      head = node;

      if (tail == null) {
        tail = head;
      }

    } else {

      var node = new Node(prev.next, value);
      prev.next = node;

      if (tail.value < value) {
        tail = node;
      }
    }
  }

  override def print() = {
    var temp: Node = head

    Console.printf("(")
    while (temp != null) {
      Console.printf(temp.value.toString() + ", ")
      temp = temp.next

    }
    Console.printf(")\n")
  }

  private def printRec(node: Node) {
    if (node != null) {
      Console.printf(node.value + ", ")
      printRec(node.next)
    }
  }

  override def printRecursion() {
    Console.printf("(")
    printRec(head)
    Console.printf(")\n")
  }

  private def printRev(node: Node) {
    if (node != null) {
      printRev(node.next)
      Console.printf(node.value.toString() + ", ")
    }
  }

  override def printReverse() {
    Console.printf("(")
    printRev(head)
    Console.printf(")\n")
  }

  override def isEmpty(): Int = {
    if (head == tail) {
      1
    }
    0
  }

  override def find(value: Conta): Conta = {
    var temp: Node = head
    var res: Conta = null
    while (temp != null) {
      if (temp.value.equals( value)) {
        res = temp.value;
      }
      temp = temp.next;
    }
    res
  }

  override def remove(value: Conta) = {
    var temp: Node = head
    var old: Node = null;

    while (temp != null) {
      if (temp.value.equals( value)) {

        if (old == null) {
          head = head.next;
        } else {
          old.next = temp.next;
        }
      }

      old = temp;
      temp = temp.next;
    }
  }

  private def removeRec(old: Node, node: Node, value: Conta) {
    if (node.value.equals(value)) {
      if (old == null) {
        head = head.next;
      } else {
        old.next = node.next;
      }
    } else {
      removeRec(node, node.next, value)
    }
  }

  override def removeRecursion(value: Conta) = {
    var old: Node = null
    var node: Node = head

    removeRec(old, node, value);
  }

  override def free() = {
    var temp: Node = head
    var node: Node = head;
    while (temp != null) {
      temp = temp.next
      node.next = null;

      node = temp
    }
  }

  def equals(list: OrderLinkedList): Boolean = {
    var headList1: Node = head
    var headList2: Node = list.head

    while (headList1 != null && headList2 != null) {
      if (headList1.value != headList2.value) {
        return false;
      }

      headList2 = headList2.next;
      headList1 = headList1.next;
    }

    if (headList1 == null && headList2 == null) {
      return true;
    } else {
      return false;
    }
  }
}

object ListTest extends App {
  println("List test")
  println("----------")

  var list: questao06.OrderLinkedList = new questao06.OrderLinkedList
  println("1- Criar uma lista vazia")
  list.create()
  println("----------")

  println("2- Inserir elemento no início")
  println("Inserindo Contas: ")

  list.insert(new Conta(1))
  list.insert(new Conta(3))
  list.insert(new Conta(2))
  println("----------")

  println("3- Imprimir os valores armazenados na lista")
  list.print()
  println("----------")

  println("4- Imprimir os valores armazenados na lista usando recursão")
  list.printRecursion()
  println("----------")

  println("5- Imprimir os valores armazenados na lista em ordem reversa (da cauda para a cabeça da lista)")
  list.printReverse()
  println("----------")

  println("6- Verificar se a lista está vazia (retorna 1 se vazia ou 0 se não vazia)")
  println("isEmpty: (" + list.isEmpty() + ")")
  println("----------")

  println("7. Recuperar/Buscar um determinado elemento da lista")
  println("find 2: (" + list.find(new Conta(2)) + ")")
  println("----------")

  println("8. Remover um determinado elemento da lista")
  println("remove: Conta(1)")
  list.remove(new Conta(1))
  list.print()
  println("----------")

  println("9. Remover um determinado elemento da lista usando recursão")
  println("remove: Conta(2)")
  list.removeRecursion(new Conta(2))
  list.print()
  println("----------")

  println("10. Liberar a lista")
  list.free()
  println("----------")

  println("11. Verificar se duas listas são iguais")
  println("lista (Conta(3)) é igual a lista(Conta(2),Conta(3))?")
  var list1: questao06.OrderLinkedList = new questao06.OrderLinkedList
  list1.create()
  list1.insert(new Conta(3))

  var list2: questao06.OrderLinkedList = new questao06.OrderLinkedList
  list2.create()
  list2.insert(new Conta(3))
  list2.insert(new Conta(2))
  println(list1.equals(list2))

  println("----------")

}
