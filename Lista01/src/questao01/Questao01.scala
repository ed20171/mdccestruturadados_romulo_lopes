package questao01 
  class Node(n: Node, v: Int) {
    var value = v
    var next = n
  }

  trait AbstractLinkedList {
    def create()
    def insert(value: Int)
    def print()
    def printRecursion()
    def printReverse()
    def isEmpty(): Int
    def find(value: Int): Int
    def remove(value: Int)
    def removeRecursion(value: Int)
    def free()
  }

  class LinkedList extends AbstractLinkedList {
    var head: Node = null
    var tail: Node = null

    override def create() {
      head = null
      tail = null
    }

    override def insert(value: Int) = {
      var node = new Node(head, value)
      head = node

      if (tail == null) {
        tail = node
      }
    }

    override def print() = {
      var temp: Node = head

      Console.printf("(")
      while (temp != null) {
        Console.printf(temp.value + ", ")
        temp = temp.next

      }
      Console.printf(")\n")
    }

    private def printRec(node: Node) {
      if (node != null) {
        Console.printf(node.value + ", ")
        printRec(node.next)
      }
    }

    override def printRecursion() {
      Console.printf("(")
      printRec(head)
      Console.printf(")\n")
    }

    private def printRev(node: Node) {
      if (node != null) {
        printRev(node.next)
        Console.printf(node.value + ", ")
      }
    }

    override def printReverse() {
      Console.printf("(")
      printRev(head)
      Console.printf(")\n")
    }

    override def isEmpty(): Int = {
      if (head == tail) {
        1
      }
      0
    }

    override def find(value: Int): Int = {
      var temp: Node = head
      var res: Int = 0
      while (temp != null) {
        if (temp.value == value) {
          res = value
        }
        temp = temp.next
      }
      res
    }

    override def remove(value: Int) = {
      var temp: Node = head
      var old: Node = null;

      while (temp != null) {
        if (temp.value == value) {

          if (old == null) {
            head = head.next;
          } else {
            old.next = temp.next;
          }
        }

        old = temp;
        temp = temp.next;
      }
    }

    private def removeRec(old: Node, node: Node, value: Int) {
      if (node.value == value) {
        if (old == null) {
          head = head.next;
        } else {
          old.next = node.next;
        }
      } else {
        removeRec(node, node.next, value)
      }
    }

    override def removeRecursion(value: Int) = {
      var old: Node = head
      var node: Node = head

      removeRec(old, node, value)
    }

    override def free() = {
      var temp: Node = head
      var node:Node = head;
      while (temp != null) {
        temp = temp.next
        node.next = null;
        
        node = temp
      }
    }
  }

object Questao01 extends App {
  println("Questão 01")
  println("----------")

  var list: questao01.LinkedList = new questao01.LinkedList()
  println("1- Criar uma lista vazia")
  list.create()
  println("----------")

  println("2- Inserir elemento no início")
  println("Inserindo os elementos: (1,2,3)")
  list.insert(1)
  list.insert(2)
  list.insert(3)
  println("----------")

  println("3- Imprimir os valores armazenados na lista")
  list.print()
  println("----------")

  println("4- Imprimir os valores armazenados na lista usando recursão")
  list.printRecursion()
  println("----------")

  println("5- Imprimir os valores armazenados na lista em ordem reversa (da cauda para a cabeça da lista)")
  list.printReverse()
  println("----------")

  println("6- Verificar se a lista está vazia (retorna 1 se vazia ou 0 se não vazia)")
  println("isEmpty: (" + list.isEmpty() + ")")
  println("----------")

  println("7. Recuperar/Buscar um determinado elemento da lista")
  println("find 2: (" + list.find(2) + ")")
  println("----------")

  println("8. Remover um determinado elemento da lista")
  println("remove: 1")
  list.remove(1)
  list.print()
  println("----------")

  println("9. Remover um determinado elemento da lista usando recursão")
  println("remove: 2")
  list.removeRecursion(2)
  list.print()
  println("----------")

  println("10. Liberar a lista")
  list.free()
  println("----------")

}

