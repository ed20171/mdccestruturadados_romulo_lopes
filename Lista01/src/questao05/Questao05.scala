package questao05

class Node(n: Node, p: Node, v: Int) {
  var value = v
  var next = n
  var prev = p
}

trait AbstractCircurlarDoublyLinkedList {
  def create()
  def insert(value: Int)
  def print()
  def printRecursion()
  def isEmpty(): Int
  def find(value: Int): Int
  def remove(value: Int)
  def removeRecursion(value: Int)
  def free()
}

class CircurlarDoublyLinkedList extends AbstractCircurlarDoublyLinkedList {
  var head: Node = null

  override def create() {
    head = null
  }

  override def insert(value: Int) = {
    if (head == null) {
      var node = new Node(null, null, value);
      node.next = node
      node.prev = node
      head = node
    } else {
      var node = new Node(head, head, value);
      head.prev = node
      head.next = node

      head = node;
    }

  }

  override def print() = {
    var temp: Node = head;
    Console.printf("(")
    do {
      Console.printf(temp.value + ", ")
      temp = temp.next;
    } while (temp != head);
    Console.printf(")\n")
  }

  private def printRec(node: Node) {
    Console.printf(node.value + ", ")
    if (node.next != head) {
      printRec(node.next)
    }
  }

  override def printRecursion() {
    Console.printf("(")
    printRec(head)
    Console.printf(")\n")
  }

  override def isEmpty(): Int = {
    if (head == null) {
      1
    }
    0
  }

  override def find(value: Int): Int = {
    var temp: Node = head
    var res: Int = 0

    do {
      if (temp.value == value) {
        res = value;
      }
      temp = temp.next;
    } while (temp != head);

    res
  }

  override def remove(value: Int) = {
    var temp: Node = head
    var old: Node = null;

    do {
      if (temp.value == value) {
        if (old == temp) {
          head = null;
        } else {
          old.next = temp.next;
        }
      }

      old = temp;
      temp = temp.next;

    } while (temp != head);
  }

  private def removeRec(old: Node, node: Node, value: Int) {
    if (node.value == value) {
      if (old == head) {
        var temp = head;
        var old = head;
        do {
          old = temp;
          temp = temp.next;

        } while (temp != head);

        head = head.next;
        old.next = head;
      } else {
        old.next = node.next;
      }
    } else {
      removeRec(node, node.next, value)
    }
  }

  override def removeRecursion(value: Int) = {
    var old: Node = head
    var node: Node = head

    removeRec(old, node, value);
  }

  override def free() = {
    var temp: Node = head
    var node: Node = head;
    while (temp != null) {
      temp = temp.next
      node.next = null;
      node.prev = null;

      node = temp
    }
  }

  def equals(list: CircurlarDoublyLinkedList): Boolean = {
    var headList1: Node = head
    var headList2: Node = list.head

    while (headList1 != null && headList2 != null) {
      if (headList1.value != headList2.value) {
        return false;
      }

      headList2 = headList2.next;
      headList1 = headList1.next;
    }
    return true;
  }
}

object Questao05 extends App {
  println("Questão 05")
  println("----------")

  var list: questao05.CircurlarDoublyLinkedList = new questao05.CircurlarDoublyLinkedList()
  println("1- Criar uma lista vazia")
  list.create()
  println("----------")

  println("2- Inserir elemento no início")
  println("Inserindo os elementos: (1,2,3)")
  list.insert(1)
  list.insert(3)
  list.insert(2)
  println("----------")

  println("3- Imprimir os valores armazenados na lista")
  list.print()
  println("----------")

  println("4- Imprimir os valores armazenados na lista usando recursão")
  list.printRecursion()
  println("----------")

  println("5- Verificar se a lista está vazia (retorna 1 se vazia ou 0 se não vazia)")
  println("isEmpty: (" + list.isEmpty() + ")")
  println("----------")

  println("6. Recuperar/Buscar um determinado elemento da lista")
  println("find 2: (" + list.find(2) + ")")
  println("----------")

  println("7. Remover um determinado elemento da lista")
  println("remove: 1")
  list.remove(1)
  list.print()
  println("----------")

  println("8. Remover um determinado elemento da lista usando recursão")
  println("remove: 2")
  list.removeRecursion(2)
  list.print()
  println("----------")

  println("9. Liberar a lista")
  list.free()
  println("----------")
}
