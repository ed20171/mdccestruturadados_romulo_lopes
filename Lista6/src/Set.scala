import scala.collection.mutable.BitSet

class Set(numElements: Int, strs: Array[String]) {
  var elements: BitSet = null

  /**
   * Cria uma um conjunto vazio
   */
  def create() {
    elements = new BitSet(numElements)
  }

  /**
   * Insere un novo elemento
   * @param value String a ser armazenada
   */
  def insert(value: String) {
    var idx = strs.indexOf(value)
    elements(idx) = true
  }

  /**
   * Remove um deteminado elemento.
   * @param value elemento que será removido
   */
  def remove(value: String) {
    var idx = strs.indexOf(value)
    elements(idx) = false
  }

  /**
   * União entre dois conjutos
   */
  def union(other: Set): Set = {
    var s = new Set(numElements, strs)
    s.elements = this.elements | other.elements
    s
  }

  /**
   * Interceção entre dois conjutos
   */
  def intersection(other: Set): Set = {
    var s = new Set(numElements, strs);
    s.elements = this.elements & other.elements
    s
  }

  /**
   * Diferença entre dois conjutos
   */
  def diff(other: Set): Set = {
    var s = new Set(numElements, strs);
    s.elements = this.elements &~ other.elements
    s
  }

  /**
   * Verifica se um conjunto A é subconjunto de B;
   */
  def subSet(other: Set): Boolean = {
    this.elements.subsetOf(other.elements)
  }

  /**
   * Verificaa se dois conjuntos são iguais;
   */
  def equals(other: Set): Boolean = {
    this.elements.equals(other.elements)
  }

  /**
   * Gera o complemento de um determinado conjunto
   */
  def complement(): Set = {
    var s = new Set(numElements, strs)
    s.create()
    for (i <- 0 until numElements) {
      s.elements(i) = true
    }

    s.elements = this.elements ^ s.elements
    s
  }

  /**
   * Verifica se um elemento pertence a um determinado conjunto
   */
  def isInside(value: String): Boolean = {
    
    var idx = strs.indexOf(value)
    elements(idx) 
  }

  /**
   * Recupera o número de elementos de um determinado conjunto
   */
  def getElements(): Int = {
    elements.size
  }

  override def toString(): String = {
    var ret = ""
    ret += "("
    elements.toList.foreach(ret += strs(_) + " , ")
    ret += ")"
    ret
  }

  /**
   * Libera a lista.
   */
  def free() {
    elements = null
  }
}

object MainSet extends App {
  println("Lista 06")
  println("============================================\n")

  var words = Array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J")
  var nBits = 10

  println("============================================\n")
  println("Criando dois conjuntos A e B")
  var setA = new Set(nBits, words)
  setA.create()

  var setB = new Set(nBits, words)
  setB.create()

  println("============================================\n")
  println("Inserindo no conjunto A ('A','B','C') e em B ('B','C','D')")
  setA.insert("A")
  setA.insert("B")
  setA.insert("C")

  setB.insert("B")
  setB.insert("C")
  setB.insert("D")

  println("Conjunto A :" + setA)
  println("Conjunto B :" + setB)

  println("============================================\n")
  println("Removendo 'B' do conjunto A ")
  setA.remove("B")
  println("Conjunto A :" + setA)
  println("Conjunto B :" + setB)

  println("============================================\n")
  println("Criando conjunto C = A U B ")
  var setC = setA.union(setB)
  println("Conjunto A :" + setA)
  println("Conjunto B :" + setB)
  println("Conjunto C :" + setC)

  println("============================================\n")

  println("Interceção entre A e B ")
  var setInterAB = setA.intersection(setB)
  println("Conjunto A :" + setA)
  println("Conjunto B :" + setB)
  println("Interceção :" + setInterAB)

  println("============================================\n")

  println("Diferença entre A e B ")
  var setDiffAB = setA.diff(setB)
  println("Conjunto A :" + setA)
  println("Conjunto B :" + setB)
  println("Diferença :" + setDiffAB)

  println("============================================\n")

  println("A é subconjunto de B ")
  println("Conjunto A :" + setA)
  println("Conjunto B :" + setB)
  println("é Subconjunto :" + setA.subSet(setB))

  println("============================================\n")

  println("A é igual ao conjunto de B ")
  println("Conjunto A :" + setA)
  println("Conjunto B :" + setB)
  println("A é igual ao B :" + setA.equals(setB))

  println("============================================\n")

  println("Complemento de A")
  println("Conjunto A :" + setA)
  println("Complemento de A :" + setA.complement())

  println("============================================\n")

  println("Elemento 'A' percentece ao Conjunto A: " + setA.isInside("A"))
  println("conjunto A: " + setA)
  println("Elemento 'A' percentece ao Conjunto B: " + setB.isInside("A"))
  println("conjunto B: " + setB)

  println("============================================\n")

  println("Numero de elementos do conjunto A: " + setA.getElements())
  println("conjunto A: " + setA)
  println("Numero de elementos do conjunto B: " + setB.getElements())
  println("conjunto B: " + setB)

}